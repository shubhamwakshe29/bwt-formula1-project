# Databricks notebook source
# MAGIC %md
# MAGIC ## Access dataframes using SQL
# MAGIC 1. create temperory views on dataframe 
# MAGIC 2. Access the view from sql cell
# MAGIC 3. Access the view from python cell

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

race_result = spark.read.parquet(f'{processed_folder_path}/race_results')

# COMMAND ----------

race_result.createOrReplaceTempView('v_race_results')

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from v_race_results
# MAGIC where race_year = 2020

# COMMAND ----------

# MAGIC %md
# MAGIC #### SQL from python cell

# COMMAND ----------

race_results_2019 = spark.sql('select * from v_race_results where race_year = 2019')

# COMMAND ----------

display(race_results_2019)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Creating global temp view..

# COMMAND ----------

# MAGIC %md
# MAGIC #### The difference between temp and global view is that we can access global view across the notebooks attached to the same cluster..

# COMMAND ----------

race_result.createGlobalTempView('gv_race_results')

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from global_temp.gv_race_results
# MAGIC where race_year = 2019

# COMMAND ----------


