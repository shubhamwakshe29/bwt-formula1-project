# Databricks notebook source
# MAGIC %md
# MAGIC 1. Write data to delta lake (managed table)
# MAGIC 2. Write data to delta lake (external table)
# MAGIC 3. Read data from delta lake (table)
# MAGIC 4. Read data from delta lake (file)

# COMMAND ----------

# MAGIC  %sql
# MAGIC  create database if not exists f1_demo
# MAGIC  location "/mnt/storageaccountformulaone/demo"

# COMMAND ----------

result_df = spark.read \
    .option("inferSchema", True) \
        .json("/mnt/storageaccountformulaone/raw/2021-03-28/results.json")

# COMMAND ----------

result_df.write.format("delta").mode("overwrite").saveAsTable("f1_demo.results_managed")


# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.results_managed;

# COMMAND ----------

result_df.write.format("delta").mode("overwrite").save("/mnt/storageaccountformulaone/demo/results_external")


# COMMAND ----------

# MAGIC %sql
# MAGIC create table f1_demo.results_external
# MAGIC using delta
# MAGIC location '/mnt/storageaccountformulaone/demo/results_external'

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.results_external;

# COMMAND ----------

results_external_df = spark.read.format("delta").load("/mnt/storageaccountformulaone/demo/results_external")

# COMMAND ----------

display(results_external_df)

# COMMAND ----------

result_df.write.format("delta").mode("overwrite").partitionBy("constructorId").saveAsTable("f1_demo.results_partitioned")

# COMMAND ----------

# MAGIC %sql
# MAGIC show partitions f1_demo.results_partitioned;

# COMMAND ----------

# MAGIC %md
# MAGIC 1. Update delta lake
# MAGIC 2. Delete from delta lake

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.results_managed;

# COMMAND ----------

# MAGIC %sql
# MAGIC update f1_demo.results_managed
# MAGIC set points = 11 - position
# MAGIC where position <= 10;

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.results_managed
# MAGIC where position <= 10;

# COMMAND ----------

from delta.tables import DeltaTable

deltaTable = DeltaTable.forPath(spark, "/mnt/storageaccountformulaone/demo/results_managed")

# Declare the predicate by using a SQL-formatted string.
deltaTable.update(
  condition = "position <= 10",
  set = { "points": "21 - position" }
)

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.results_managed
# MAGIC where position <= 10;

# COMMAND ----------

# MAGIC %sql
# MAGIC delete from f1_demo.results_managed
# MAGIC where position > 10;

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.results_managed;
# MAGIC

# COMMAND ----------

from delta.tables import DeltaTable

deltaTable = DeltaTable.forPath(spark, "/mnt/storageaccountformulaone/demo/results_managed")

# Declare the predicate by using a SQL-formatted string.
deltaTable.delete(
  condition = "points = 0",
)

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.results_managed;

# COMMAND ----------

# MAGIC %md
# MAGIC ### Upsert using merge

# COMMAND ----------

drivers_day1_df = spark.read \
    .option("inferSchema", True) \
        .json("/mnt/storageaccountformulaone/raw/2021-03-28/drivers.json") \
            .filter("driverId <= 10") \
                .select("driverId","dob","name.forename","name.surname")

# COMMAND ----------

drivers_day1_df.createOrReplaceTempView("drivers_d1")

# COMMAND ----------

display(drivers_day1_df)

# COMMAND ----------

from pyspark.sql.functions import upper

drivers_day2_df = spark.read \
    .option("inferSchema", True) \
        .json("/mnt/storageaccountformulaone/raw/2021-03-28/drivers.json") \
            .filter("driverId between 6 and 15") \
                .select("driverId","dob",upper("name.forename").alias("forename"),upper("name.surname").alias("surname"))

# COMMAND ----------

drivers_day2_df.createOrReplaceTempView("drivers_d2")

# COMMAND ----------

display(drivers_day2_df)

# COMMAND ----------

from pyspark.sql.functions import upper

drivers_day3_df = spark.read \
    .option("inferSchema", True) \
        .json("/mnt/storageaccountformulaone/raw/2021-03-28/drivers.json") \
            .filter("driverId between 1 and 5 or driverId between 16 and 20") \
                .select("driverId","dob",upper("name.forename").alias("forename"),upper("name.surname").alias("surname"))

# COMMAND ----------

display(drivers_day3_df)

# COMMAND ----------

# MAGIC %sql
# MAGIC create table if not exists f1_demo.drivers_merge(
# MAGIC   driverId int,
# MAGIC   dob date,
# MAGIC   forename string,
# MAGIC   surname string,
# MAGIC   createdDate date,
# MAGIC   updatedDate date
# MAGIC )
# MAGIC using delta

# COMMAND ----------

# MAGIC %sql
# MAGIC MERGE INTO f1_demo.drivers_merge tgt
# MAGIC USING drivers_d1 upd
# MAGIC ON tgt.driverId = upd.driverId
# MAGIC WHEN MATCHED THEN 
# MAGIC update set tgt.dob = upd.dob,
# MAGIC            tgt.forename = upd.forename,
# MAGIC            tgt.surname = upd.surname,
# MAGIC            tgt.updatedDate = current_timestamp
# MAGIC WHEN NOT MATCHED 
# MAGIC THEN insert (driverId, dob, forename, surname, createdDate) values (driverId, dob, forename, surname, current_timestamp)
# MAGIC

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.drivers_merge;

# COMMAND ----------

# MAGIC %md
# MAGIC #### Day2

# COMMAND ----------

# MAGIC %sql
# MAGIC MERGE INTO f1_demo.drivers_merge tgt
# MAGIC USING drivers_d2 upd
# MAGIC ON tgt.driverId = upd.driverId
# MAGIC WHEN MATCHED THEN 
# MAGIC update set tgt.dob = upd.dob,
# MAGIC            tgt.forename = upd.forename,
# MAGIC            tgt.surname = upd.surname,
# MAGIC            tgt.updatedDate = current_timestamp
# MAGIC WHEN NOT MATCHED 
# MAGIC THEN insert (driverId, dob, forename, surname, createdDate) values (driverId, dob, forename, surname, current_timestamp)

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.drivers_merge;

# COMMAND ----------

from pyspark.sql.functions import current_timestamp
from delta.tables import DeltaTable

deltaTablePeople = DeltaTable.forPath(spark, '/mnt/storageaccountformulaone/demo/drivers_merge')

deltaTablePeople.alias('tgt') \
  .merge(
    drivers_day3_df.alias('upd'),
    'tgt.driverId = upd.driverId'
  ) \
  .whenMatchedUpdate(set =
    {
      "tgt.dob": "upd.dob",
      "tgt.forename": "upd.forename",
      "tgt.surname": "upd.surname",
      "updatedDate": "current_timestamp()"
    }
  ) \
  .whenNotMatchedInsert(values =
    {
      "driverId":"upd.driverId",
      "dob" : "upd.dob",
      "forename": "upd.forename",
      "surname": "upd.surname",
      "createdDate": "current_timestamp()"
    }
  ) \
  .execute()      

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.drivers_merge;

# COMMAND ----------

# MAGIC %md
# MAGIC 1. History and Versioning
# MAGIC 2. Time travel
# MAGIC 3. Vaccum

# COMMAND ----------

# MAGIC %sql
# MAGIC desc history f1_demo.drivers_merge;

# COMMAND ----------

# MAGIC %md How to do versioning

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.drivers_merge version as of 2;

# COMMAND ----------

df = spark.read.format("delta").option("versionAsOf", "1").load("/mnt/storageaccountformulaone/demo/drivers_merge")

# COMMAND ----------

display(df)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Vaccume delete the history which is older than 7 days..

# COMMAND ----------

# MAGIC %sql
# MAGIC set spark.databricks.delta.retentionDurationCheck.enabled = False;
# MAGIC vacuum f1_demo.drivers_merge retain 0 hours;

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.drivers_merge version as of 2;

# COMMAND ----------

# MAGIC %md After perfroming vacuum if you try to fecth whole table using select query you will be able to see the table(with latest changes) but no means of going back. 

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.drivers_merge 

# COMMAND ----------

# MAGIC %sql
# MAGIC desc history f1_demo.drivers_merge ;

# COMMAND ----------

# MAGIC %sql
# MAGIC delete from f1_demo.drivers_merge 
# MAGIC where driverId = 1;

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_demo.drivers_merge version as of 4;

# COMMAND ----------

# MAGIC %md
# MAGIC #### Transaction Logs

# COMMAND ----------

# MAGIC %md - Transaction logs are not kept within hive metastore
# MAGIC - Transaction logs are kept for 30 days..

# COMMAND ----------

# MAGIC %sql
# MAGIC create table if not exists f1_demo.drivers_txn(
# MAGIC   driverId int,
# MAGIC   dob date,
# MAGIC   forename string,
# MAGIC   surname string,
# MAGIC   createdDate date,
# MAGIC   updatedDate date
# MAGIC )

# COMMAND ----------

# MAGIC %sql
# MAGIC desc history f1_demo.drivers_txn;

# COMMAND ----------

# MAGIC %sql
# MAGIC insert into f1_demo.drivers_txn
# MAGIC select * from f1_demo.drivers_merge
# MAGIC where driverId = 1;

# COMMAND ----------

# MAGIC %sql
# MAGIC desc history f1_demo.drivers_txn;

# COMMAND ----------

# MAGIC %md
# MAGIC

# COMMAND ----------

# MAGIC %sql
# MAGIC create table if not exists f1_demo.drivers_convert_to_delta(
# MAGIC   driverId int,
# MAGIC   dob date,
# MAGIC   forename string,
# MAGIC   surname string,
# MAGIC   createdDate date,
# MAGIC   updatedDate date
# MAGIC )
# MAGIC using parquet

# COMMAND ----------

# MAGIC  %sql
# MAGIC  insert into f1_demo.drivers_convert_to_delta
# MAGIC  select * from f1_demo.drivers_merge;

# COMMAND ----------

# MAGIC %sql
# MAGIC convert to delta f1_demo.drivers_convert_to_delta;

# COMMAND ----------

df = spark.table("f1_demo.drivers_convert_to_delta")

# COMMAND ----------

df.write.format("parquet").save("/mnt/storageaccountformulaone/demo/drivers_convert_to_delta_new")

# COMMAND ----------

# MAGIC %sql
# MAGIC
# MAGIC convert to delta parquet. `/mnt/storageaccountformulaone/demo/drivers_convert_to_delta_new`

# COMMAND ----------


