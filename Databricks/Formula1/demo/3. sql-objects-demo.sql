-- Databricks notebook source
-- MAGIC %md
-- MAGIC ## Lesson Objectives
-- MAGIC 1. Spark SQL documentation
-- MAGIC 2. create databse demo
-- MAGIC 3. data tab in th UI
-- MAGIC 4. SHOW command
-- MAGIC 5. DESCRIBE command
-- MAGIC 6. Find the current database

-- COMMAND ----------

-- MAGIC %run "../includes/configuration"

-- COMMAND ----------

CREATE DATABASE IF NOT EXISTS demo

-- COMMAND ----------

SHOW DATABASES;

-- COMMAND ----------

DESCRIBE DATABASE demo

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### Creating a managed table
-- MAGIC 1. Create manage table using python
-- MAGIC 2. create managed table using SQL
-- MAGIC 3. Effect of dropping a managed table
-- MAGIC 4. describe table

-- COMMAND ----------

-- MAGIC %run ../includes/configuration

-- COMMAND ----------

-- MAGIC %python
-- MAGIC race_result = spark.read.parquet(f'{processed_folder_path}/race_results')

-- COMMAND ----------

-- MAGIC %python
-- MAGIC race_result.write.format('parquet').saveAsTable('demo.race_result_table')

-- COMMAND ----------

USE demo;
SHOW TABLES;

-- COMMAND ----------

DESC EXTENDED race_result_table;

-- COMMAND ----------

SELECT * FROM demo.race_result_table;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create managed table using sql..

-- COMMAND ----------

CREATE TABLE demo.race_result_sql
AS
SELECT * FROM demo.race_result_table

-- COMMAND ----------

SELECT * FROM demo.race_result_sql

-- COMMAND ----------

DESC EXTENDED demo.race_result_sql

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### creating external table..
-- MAGIC 1. create extrernal table using python
-- MAGIC 2. create external table using SQL
-- MAGIC 3. effect of dropping an external table

-- COMMAND ----------

-- MAGIC %python
-- MAGIC race_result.write.format('parquet').option('path',f'{presentation_folder_path}/race_result_ext_py').saveAsTable('demo.race_result_ext_py')

-- COMMAND ----------

USE demo

-- COMMAND ----------

DESC EXTENDED race_result_ext_py

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### external table using sql
-- MAGIC

-- COMMAND ----------

CREATE TABLE IF NOT EXISTS demo.race_result_ext_sql
(
  race_year INT,
  race_name STRING,
  race_date TIMESTAMP,
  circuit_location STRING,
  driver_name STRING,
  driver_number INT,
  driver_nationality STRING,
  team_name STRING,
  grid INT,
  fastest_lap INT,
  race_time STRING,
  points FLOAT,
  position INT,
  created_date TIMESTAMP
)
USING parquet
LOCATION "/mnt/storageaccountformulaone/presentation/race_result_ext_sql"

-- COMMAND ----------

SHOW TABLES IN demo;

-- COMMAND ----------

desc extended demo.race_result_ext_sql

-- COMMAND ----------

DROP TABLE demo.race_result_ext_sql

-- COMMAND ----------

DROP TABLE demo.race_result_ext_sql

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ### Views on table..
-- MAGIC #### Learning objectives
-- MAGIC 1. create temp view
-- MAGIC 2. create global temp view
-- MAGIC 3. create permanent view

-- COMMAND ----------

CREATE OR REPLACE TEMP VIEW v_race_result
AS
SELECT * FROM demo.race_result_table

-- COMMAND ----------

SELECT * FROM v_race_result

-- COMMAND ----------

CREATE OR REPLACE GLOBAL TEMP VIEW gv_race_result
AS
SELECT * FROM demo.race_result_table
WHERE race_year = 2018

-- COMMAND ----------

SELECT * FROM global_temp.gv_race_result

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### creating a permanent view..

-- COMMAND ----------

CREATE OR REPLACE VIEW demo.p_race_result
AS
SELECT * FROM demo.race_result_table
WHERE race_year = 2012

-- COMMAND ----------


