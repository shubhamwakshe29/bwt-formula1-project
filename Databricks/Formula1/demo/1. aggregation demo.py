# Databricks notebook source
# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %md
# MAGIC ### Built-in aggregate functions..

# COMMAND ----------

# MAGIC %md
# MAGIC #### Use aggregate functions with select function..

# COMMAND ----------

race_result_df = spark.read.parquet(f'{processed_folder_path}/race_results')

# COMMAND ----------

display(race_result_df)

# COMMAND ----------

race_result_demo = race_result_df.filter('race_year in (2019,2020)')

# COMMAND ----------

race_result_demo.display()

# COMMAND ----------

from pyspark.sql.functions import count, countDistinct, sum, max

# COMMAND ----------

race_result_demo.select(countDistinct('driver_name')).show()

# COMMAND ----------

race_result_demo.filter('driver_name = "Lewis Hamilton"').select(sum('points').alias('LH points')
                                                                 ,countDistinct('race_name').alias('LH races')).show()

# COMMAND ----------

# MAGIC %md
# MAGIC #### GroupBy function.. 

# COMMAND ----------

demo_grouped_df = race_result_demo.groupBy('race_year','driver_name').agg(sum('points').alias('total_points'),countDistinct('race_name').alias('number_of_races'))

# COMMAND ----------

display(demo_grouped_df)

# COMMAND ----------

# MAGIC %md
# MAGIC #### window functions..

# COMMAND ----------

from pyspark.sql.window import Window
from pyspark.sql.functions import desc, rank

# COMMAND ----------

window_spec = Window.partitionBy('race_year').orderBy(desc('total_points'))

# COMMAND ----------

demo_grouped_df.withColumn('rank',rank().over(window_spec)).show()

# COMMAND ----------


