# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get('p_data_source')

# COMMAND ----------

dbutils.widgets.text("v_file_date","2021-03-21")
v_file_date = dbutils.widgets.get('v_file_date')

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 1. Read the json file using dataframe reader API

# COMMAND ----------

from pyspark.sql.types import StructType, StringType, IntegerType, DateType, StructField

# COMMAND ----------

name_schema = StructType([StructField('forename', StringType(), True),
                          StructField('surname',StringType(), True)

])

# COMMAND ----------

drivers_schema = StructType([StructField('driverId',IntegerType(), False),
                             StructField('driverRef',StringType(), True),
                             StructField('number',IntegerType(), True),
                             StructField('code', StringType(), True),
                             StructField('name',name_schema, True),
                             StructField('dob', DateType(), True),
                             StructField('nationality',StringType(), True),
                             StructField('url',StringType(),True)

])

# COMMAND ----------

drivers_df = spark.read.json(f'{raw_folder_path}/{v_file_date}/drivers.json',schema= drivers_schema)

# COMMAND ----------

display(drivers_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 2. Rename columns and add new columns
# MAGIC 1. driverID renamed to driver_id
# MAGIC 2. driverRef renamed to driver_ref
# MAGIC 3. Ingestion date added
# MAGIC 4. name added with concatenation of forename and surname

# COMMAND ----------

from pyspark.sql.functions import col, lit, concat, current_timestamp

# COMMAND ----------

drivers_with_column_rename = ingestion_date(drivers_df).withColumnRenamed('driverID','driver_id')\
                                        .withColumnRenamed('driverRef','driver_ref')\
                                                .withColumn('name', concat(col('name.forename'), lit(' '), col('name.surname')))\
                                                .withColumn("data_source",lit(v_data_source))\
                                                .withColumn("file_date",lit(v_file_date))

# COMMAND ----------

display(drivers_with_column_rename)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step 3. Drop unwanted columns..

# COMMAND ----------

drivers_final_df = drivers_with_column_rename.drop('url')

# COMMAND ----------

display(drivers_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 4. Write the output into the proessed container in parquet file..

# COMMAND ----------

drivers_final_df.write.mode('overwrite').format("delta").saveAsTable("f1_processed.drivers")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.drivers;

# COMMAND ----------


