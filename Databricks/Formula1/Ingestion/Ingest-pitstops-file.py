# Databricks notebook source
# MAGIC %md
# MAGIC ## Step 1. Read json file using dataframe reader API

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, StringType, IntegerType, LongType

# COMMAND ----------

pit_stops_schema = StructType([StructField('raceId', IntegerType(), False),
                               StructField('driverId', IntegerType(), False),
                               StructField('stop', StringType(), False),
                               StructField('lap', IntegerType(), False),
                               StructField('time', StringType(), False),
                               StructField('duration', StringType(), False),
                               StructField('milliseconds', IntegerType(), False)

])

# COMMAND ----------

pit_stops_df = spark.read.json(f'{raw_folder_path}/{v_file_date}/pit_stops.json',schema= pit_stops_schema,multiLine=True)

# COMMAND ----------

display(pit_stops_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 2. Rename columns and add new column

# COMMAND ----------

from pyspark.sql.functions import current_timestamp, lit

# COMMAND ----------

pit_stops_final_df = ingestion_date(pit_stops_df).withColumnRenamed('raceID','race_id')\
                                   .withColumnRenamed('driverID','driver_id')\
                                       .withColumn("data_source",lit(v_data_source))\
                                           .withColumn("file_date",lit(v_file_date))                                    

# COMMAND ----------

display(pit_stops_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 3. Write a final output in processed container in parquet format

# COMMAND ----------

# overwrite_partition(pit_stops_final_df,"f1_processed","pit_stops","race_id")

# COMMAND ----------

merge_condition = "tgt.race_id = src.race_id and tgt.driver_id = src.driver_id and tgt.stop = src.stop and tgt.race_id = src.race_id"
merge_delta_data(pit_stops_final_df, "f1_processed", "pit_stops", processed_folder_path, merge_condition, "race_id")

# COMMAND ----------

# pit_stops_final_df.write.mode('overwrite').format("parquet").saveAsTable("f1_processed.pit_stops")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id, count(1)
# MAGIC from f1_processed.pit_stops
# MAGIC group by race_id
# MAGIC order by race_id desc;

# COMMAND ----------

# display(spark.read.parquet("/mnt/storageaccountformulaone/processed/pit_stops"))

# COMMAND ----------


