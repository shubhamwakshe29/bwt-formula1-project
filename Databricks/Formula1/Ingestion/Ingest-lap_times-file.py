# Databricks notebook source
# MAGIC %md
# MAGIC ## Step 1. Read csv file using spark dataframe reader API..

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, StringType, IntegerType

# COMMAND ----------

lap_times_schema = StructType([StructField('raceId', IntegerType(), False),
                               StructField('driverId', IntegerType(), False),
                               StructField('lap', IntegerType(), True),
                               StructField('position', IntegerType(), True),
                               StructField('time', StringType(), True),
                               StructField('milliseconds', IntegerType(), True)

])

# COMMAND ----------

lap_times_df = spark.read.csv(f'{raw_folder_path}/{v_file_date}/lap_times',schema= lap_times_schema)

# COMMAND ----------

display(lap_times_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 2. Rename required columns and add new column

# COMMAND ----------

from pyspark.sql.functions import lit

# COMMAND ----------

lap_times_final_df = ingestion_date(lap_times_df).withColumnRenamed('raceID','race_id')\
                                 .withColumnRenamed('driverID','driver_id')\
                                     .withColumn("data_source",lit(v_data_source))\
                                         .withColumn("file_date",lit(v_file_date))
                                   

# COMMAND ----------

display(lap_times_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 3. Write the output in processed container in parquet format..

# COMMAND ----------

# overwrite_partition(lap_times_final_df,"f1_processed","lap_times",'race_id')

# COMMAND ----------

merge_condition = "tgt.race_id = src.race_id and tgt.driver_id = src.driver_id and tgt.lap = src.lap and tgt.race_id = src.race_id"
merge_delta_data(lap_times_final_df, "f1_processed", "lap_times", processed_folder_path, merge_condition, "race_id")

# COMMAND ----------

# lap_times_final_df.write.mode('overwrite').format("parquet").saveAsTable("f1_processed.lap_times")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id, count(1) 
# MAGIC from f1_processed.lap_times
# MAGIC group by race_id
# MAGIC order by race_id desc

# COMMAND ----------

# display(spark.read.parquet('/mnt/storageaccountformulaone/processed/lap_times'))

# COMMAND ----------


