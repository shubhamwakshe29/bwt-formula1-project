# Databricks notebook source
# MAGIC %md
# MAGIC ## Step 1. Read json file using spark dataframe reader API..

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, StringType, IntegerType

# COMMAND ----------

qualifying_schema = StructType([StructField('qualifyId', IntegerType(), False),
                               StructField('raceId', IntegerType(), True),
                               StructField('driverId', IntegerType(), True),
                               StructField('constructorId', IntegerType(), True),
                               StructField('number', IntegerType(), True),
                               StructField('position', IntegerType(), True),
                               StructField('q1', StringType(), True),
                               StructField('q2', StringType(), True),
                               StructField('q3', StringType(), True)

])

# COMMAND ----------

qualifying_df = spark.read.json(f'{raw_folder_path}/{v_file_date}/qualifying',schema= qualifying_schema,
                                multiLine=  True)

# COMMAND ----------

display(qualifying_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 2. Rename required columns and add new column

# COMMAND ----------

from pyspark.sql.functions import current_timestamp, lit

# COMMAND ----------

qualifying_final_df = ingestion_date(qualifying_df).withColumnRenamed('qualifyId','qualify_id')\
                                 .withColumnRenamed('raceID','race_id')\
                                 .withColumnRenamed('driverID','driver_id')\
                                 .withColumnRenamed('constructorID','constructor_id')\
                                 .withColumn("data_source",lit(v_data_source))\
                                 .withColumn("file_date",lit(v_file_date))                                      

# COMMAND ----------

display(qualifying_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 3. Write the output in processed container in parquet format..

# COMMAND ----------

# overwrite_partition(qualifying_final_df,"f1_processed","qualifying","race_id")

# COMMAND ----------

merge_condition = "tgt.qualify_id = src.qualify_id and tgt.race_id = src.race_id"
merge_delta_data(qualifying_final_df, "f1_processed", "qualifying", processed_folder_path, merge_condition, "race_id")

# COMMAND ----------

# qualifying_final_df.write.mode('overwrite').format("parquet").saveAsTable("f1_processed.qualifying")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id, count(1) 
# MAGIC from f1_processed.qualifying
# MAGIC group by race_id
# MAGIC order by race_id desc

# COMMAND ----------

# display(spark.read.parquet('/mnt/storageaccountformulaone/processed/qualifying'))

# COMMAND ----------


