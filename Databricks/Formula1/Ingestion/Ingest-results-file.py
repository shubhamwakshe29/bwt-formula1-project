# Databricks notebook source
# MAGIC %md
# MAGIC ## Step 1. Read the file using dataframe API..

# COMMAND ----------

dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-28")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType, FloatType

# COMMAND ----------

results_schema = StructType([StructField('resultId', IntegerType(), False),
                             StructField('raceId', IntegerType(), True),
                             StructField('driverId', IntegerType(), True),
                             StructField('constructorId', IntegerType(), True),
                             StructField('number', IntegerType(), False),
                             StructField('grid', IntegerType(), False),
                             StructField('position', IntegerType(), False),
                             StructField('positionText', StringType(), False),
                             StructField('positionOrder', IntegerType(), False),
                             StructField('points', FloatType(), False),
                             StructField('laps', IntegerType(), False),
                             StructField('time', StringType(), False),
                             StructField('milliseconds', IntegerType(), False),
                             StructField('fastestLap', IntegerType(), False),
                             StructField('rank', IntegerType(), False),
                             StructField('fastestLapTime', StringType(), False),
                             StructField('fastestLapSpeed', StringType(), False),
                             StructField('statusId', IntegerType(), False)
                             

])

# COMMAND ----------

results_df = spark.read.json(f'{raw_folder_path}/{v_file_date}/results.json', schema= results_schema)

# COMMAND ----------

results_df.count()

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 2. Droping unwated columns and renaming some of the columns..

# COMMAND ----------

from pyspark.sql.functions import lit

# COMMAND ----------

results_final_df = results_df.withColumnRenamed('resultID','result_id')\
                             .withColumnRenamed('raceID','race_id')\
                             .withColumnRenamed('driverID','driver_id')\
                             .withColumnRenamed('constructorID','constructor_id')\
                             .withColumnRenamed('positionText','position_text')\
                             .withColumnRenamed('positionOrder','position_order')\
                             .withColumnRenamed('fastestLap','fastest_lap')\
                             .withColumnRenamed('fastestLapTime','fastest_lap_time')\
                             .withColumnRenamed('fastestLapSpeed','fastest_lap_speed')\
                             .drop('statusID')\
                             .withColumn("data_source",lit(v_data_source))\
                                 .withColumn("file_date",lit(v_file_date))        

                             

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 3. add ingestion date column..

# COMMAND ----------

results_final_df = ingestion_date(results_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC #### de-dupe the dataframe

# COMMAND ----------

results_deduped_df = results_final_df.dropDuplicates(["race_id","driver_id"])

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 4. Write the output in processed container in parquet format..

# COMMAND ----------

# MAGIC %md 
# MAGIC #### Method one of incremental load

# COMMAND ----------

# for race_id_list in results_final_df.select('race_id').distinct().collect():
#     if (spark._jsparkSession.catalog().tableExists("f1_processed.results")):
#         spark.sql(f"alter table f1_processed.results drop if exists partition (race_id = {race_id_list.race_id})")

# COMMAND ----------

# results_final_df.write.mode('append').partitionBy('race_id').format("parquet")\
#     .saveAsTable("f1_processed.results")

# COMMAND ----------

# display(spark.read.parquet('/mnt/storageaccountformulaone/processed/results'))

# COMMAND ----------

# MAGIC %md
# MAGIC #### Method 2 of incremental load

# COMMAND ----------

# MAGIC %sql
# MAGIC -- drop table f1_processed.results;

# COMMAND ----------

# overwrite_partition(results_final_df,"f1_processed","results","race_id")

# COMMAND ----------

merge_condition = "tgt.result_id = src.result_id and tgt.race_id = src.race_id"
merge_delta_data(results_deduped_df, "f1_processed", "results", processed_folder_path, merge_condition, "race_id")

# COMMAND ----------

# MAGIC %sql
# MAGIC select count(1)
# MAGIC from f1_processed.results;

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id, driver_id, count(1)
# MAGIC from f1_processed.results
# MAGIC group by race_id,driver_id
# MAGIC having count(1)>1
# MAGIC order by race_id desc

# COMMAND ----------


