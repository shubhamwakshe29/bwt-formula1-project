# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 1. Reading constructor jason file..
# MAGIC ### Reading schema from DDL..

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, IntegerType, StringType, LongType

# COMMAND ----------

constructor_schema1 = StructType([StructField('constructorId', IntegerType(), True),
                                  StructField('constructorRef', StringType(), True),
                                  StructField('name', StringType(), True),
                                  StructField('nationality', StringType(), True),
                                  StructField('url', StringType(), True)
                                  
])

# COMMAND ----------

constructor_df = spark.read.schema(constructor_schema1).json(f'{raw_folder_path}/{v_file_date}/constructors.json')

# COMMAND ----------

display(constructor_df)

# COMMAND ----------

constructor_df.printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step 2. Drop unwanted column from dataframe

# COMMAND ----------

constructor_selected_df = constructor_df.drop('url')

# COMMAND ----------

display(constructor_selected_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step 3. Rename column and add new column named ingestion_date

# COMMAND ----------

from pyspark.sql.functions import current_timestamp, lit

# COMMAND ----------

constructor_final_df = ingestion_date(constructor_selected_df).withColumnRenamed('constructorID','constructor_id')\
                       .withColumnRenamed('constructorRef','constructor_ref')\
                           .withColumn("data_source",lit(v_data_source))\
                           .withColumn("file_date",lit(v_file_date))
                       

# COMMAND ----------

display(constructor_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step 4. Write final data to parquet file..

# COMMAND ----------

constructor_final_df.write.mode('overwrite').format("delta").saveAsTable("f1_processed.constructor")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.constructor;
