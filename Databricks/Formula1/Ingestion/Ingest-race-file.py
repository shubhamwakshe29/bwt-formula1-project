# Databricks notebook source
dbutils.widgets.text("p_data_source","")
v_data_source = dbutils.widgets.get("p_data_source")

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

# MAGIC %md 
# MAGIC ## Step 1. Read the csv file using spark dataframe reader

# COMMAND ----------

# MAGIC %md
# MAGIC #### Creating schema

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DateType

# COMMAND ----------

race_schema = StructType([StructField('raceId', IntegerType(), False),
                          StructField('year', IntegerType(), True),
                          StructField('round', IntegerType(), True),
                          StructField('circuitId', IntegerType(), True),
                          StructField('name', StringType(), True),
                          StructField('date', DateType(), True),
                          StructField('time', StringType(), True),
                          StructField('url', StringType(), True)
                                

])

# COMMAND ----------

race_df = spark.read.csv(f'{raw_folder_path}/{v_file_date}/races.csv', header= True, schema= race_schema)

# COMMAND ----------

race_df.show()

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step 2. Add ingestion date and race_timestamp in a dataframe

# COMMAND ----------

from pyspark.sql.functions import current_timestamp, to_timestamp, concat, col, lit

# COMMAND ----------

races_with_timestamp_df = ingestion_date(race_df)\
                                 .withColumn('race_timestamp', to_timestamp(concat(col('date'),lit(' '),col('time')),"yyyy-MM-dd HH:mm:ss"))\
                                     .withColumn('data_source', lit(v_data_source))\
                                         .withColumn("file_date", lit(v_file_date))

# COMMAND ----------

display(races_with_timestamp_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 3. Select only the required columns

# COMMAND ----------

races_final_df = races_with_timestamp_df.select(col("raceId").alias('race_id'),col('year').alias('race_year'),col('round'),col('circuitId').alias('circuit_id'),col('name'),col('race_timestamp'),col('ingestion_date'))

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 4. Write the output in a processed container in parquet format..

# COMMAND ----------

races_final_df.write.mode('overwrite').partitionBy('race_year').format("delta")\
    .saveAsTable("f1_processed.races")

# COMMAND ----------

# MAGIC %sql
# MAGIC  select * from f1_processed.races;

# COMMAND ----------


