# Databricks notebook source
# MAGIC %md
# MAGIC # Ingest circuits.csv file

# COMMAND ----------

dbutils.widgets.text('p_data_source','')
v_data_source = dbutils.widgets.get('p_data_source')

# COMMAND ----------

dbutils.widgets.text('p_file_date','2021-03-21')
v_file_date = dbutils.widgets.get('p_file_date')

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 1. Read the CSV file using spark dataframe reader

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DoubleType

# COMMAND ----------

from pyspark.sql.functions import col

# COMMAND ----------

circuits_schema = StructType([StructField('circuitId', IntegerType(), False),
                              StructField('circuitRef', StringType(), True),
                              StructField('name', StringType(), True),
                              StructField('location', StringType(), True),
                              StructField('country', StringType(), True),
                              StructField('lat', DoubleType(), True),
                              StructField('lng', DoubleType(), True),
                              StructField('alt', IntegerType(), True),
                              StructField('url', StringType(), True)

                              
                              ])

# COMMAND ----------

circuits_df = spark.read \
        .option('header', True)\
        .schema(circuits_schema)\
        .csv(f'{raw_folder_path}/{v_file_date}/circuits.csv')

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 2. Select required columns

# COMMAND ----------

circuits_selected_df = circuits_df.select(col('circuitId'),col('circuitRef'),col('name'),col('location'),
                   col('country'),col('lat'),
                   col('lng'),col('alt'))

# COMMAND ----------

# MAGIC %md
# MAGIC ## step 3. Rename columns

# COMMAND ----------

from pyspark.sql.functions import lit

# COMMAND ----------

circuit_renamed_df = circuits_selected_df.withColumnRenamed('circuitID','circuit_id') \
    .withColumnRenamed('circuitRef','circuit_ref') \
        .withColumnRenamed('lat','latitude') \
            .withColumnRenamed('lng','longitude') \
                .withColumnRenamed('alt','altitude')\
                    .withColumn('data_source',lit(v_data_source))\
                        .withColumn('file_date',lit(v_file_date))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Step 4. Add ingestion date to the dataframe..

# COMMAND ----------

circuit_final_df = ingestion_date(circuit_renamed_df)

# COMMAND ----------

display(circuit_final_df)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Step 5. Write dataframe to data lake as parquet

# COMMAND ----------

circuit_final_df.write.mode('overwrite').format("delta").saveAsTable("f1_processed.circuits")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_processed.circuits;
