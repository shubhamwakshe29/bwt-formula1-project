# Databricks notebook source
# MAGIC %run ./includes/configuration

# COMMAND ----------

df = spark.read.parquet(f'{processed_folder_path}/race_results')

# COMMAND ----------

display(df)

# COMMAND ----------

# MAGIC %md
# MAGIC ##### 1. Get a driver name who has maximum point per year

# COMMAND ----------

from pyspark.sql.window import Window
from pyspark.sql.functions import max, desc, dense_rank,asc, regexp_replace, col


# COMMAND ----------

window_spec = Window.partitionBy('race_year').orderBy(desc('points'))

# COMMAND ----------

df1 = df.withColumn('dense_rank',dense_rank().over(window_spec))

# COMMAND ----------

display(df1.select('driver_name','points','race_year','dense_rank').filter('dense_rank = 1').distinct())

# COMMAND ----------

# MAGIC %md
# MAGIC #### 2. Get a team name who has scored Max point each year
# MAGIC

# COMMAND ----------

display(df1.select('race_year','team_name','points','dense_rank').filter('dense_rank = 1').distinct())

# COMMAND ----------

# MAGIC %md
# MAGIC #### 3. Get a driver name and its team name who has covered the qualifying race in minimum time (q1)

# COMMAND ----------

qualifying_df = spark.read.parquet(f'{processed_folder_path}/qualifying')

# COMMAND ----------

qualifying_df = qualifying_df.withColumn('q1',regexp_replace('q1','\\\\N',' '))

# COMMAND ----------

display(qualifying_df)

# COMMAND ----------

race_qualifying = df.join(qualifying_df,df.race_id == qualifying_df.race_id)

# COMMAND ----------

display(race_qualifying)

# COMMAND ----------

final_spec = Window.orderBy(asc('q1'))

# COMMAND ----------

final_df = race_qualifying.select('driver_name','team_name','q1')\
    .withColumn('d_rank',dense_rank().over(final_spec))

# COMMAND ----------

display(final_df)

# COMMAND ----------


