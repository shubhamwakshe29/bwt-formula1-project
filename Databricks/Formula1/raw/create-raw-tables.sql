-- Databricks notebook source
-- MAGIC %md
-- MAGIC ###### Create a database..

-- COMMAND ----------

CREATE DATABASE IF NOT EXISTS f1_raw;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create a circuits table;

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.circuits;
CREATE TABLE IF NOT EXISTS f1_raw.circuits(
  circuitId INT,
  circuitRef STRING,
  name STRING,
  location STRING,
  country STRING,
  lat DOUBLE,
  lng DOUBLE,
  alt INT,
  url STRING

)
USING csv
OPTIONS (path "/mnt/storageaccountformulaone/raw/circuits.csv",header= True)

-- COMMAND ----------

USE f1_raw;
SELECT * FROM circuits;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create races table

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.races;
CREATE TABLE IF NOT EXISTS f1_raw.races(
  raceId INT,
  year INT,
  round INT,
  circuitId INT,
  name STRING,
  date DATE,
  time STRING,
  url STRING
)
USING csv 
OPTIONS (path "/mnt/storageaccountformulaone/raw/races.csv", header = True);

-- COMMAND ----------

SELECT * FROM f1_raw.races;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ### create tables for json files..

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create constructors table..
-- MAGIC - single line table
-- MAGIC - simple structure

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.constructors;
CREATE TABLE IF NOT EXISTS f1_raw.constructors(
  constructorId INT,
  constructorRef STRING,
  name STRING,
  nationality STRING,
  url STRING
)
USING Json 
OPTIONS (path "/mnt/storageaccountformulaone/raw/constructors.json")

-- COMMAND ----------

SELECT * FROM f1_raw.constructors;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create drivers table
-- MAGIC - single line json
-- MAGIC - complex structure

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.drivers;
CREATE TABLE IF NOT EXISTS f1_raw.drivers(
  driverId INT,
  driverRef STRING,
  number INT,
  code STRING,
  name STRUCT<forename : STRING, surname: STRING>,
  dob DATE,
  nationality STRING,
  url STRING
)
USING json
OPTIONS (path "/mnt/storageaccountformulaone/raw/drivers.json")

-- COMMAND ----------

SELECT * FROM f1_raw.drivers

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create results table
-- MAGIC - sinle line json
-- MAGIC - simple structure

-- COMMAND ----------

-- MAGIC %python
-- MAGIC df = spark.read.json("/mnt/storageaccountformulaone/raw/results.json")

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.results;
CREATE TABLE IF NOT EXISTS f1_raw.results(
  resultId INT,
  raceId INT,
  driverId INT,
  constructorId INT,
  number INT,
  grid INT,
  position INT,
  positionText STRING,
  positionOrder STRING,
  points INT,
  laps INT,
  time STRING,
  milliseconds INT,
  fastestLap INT,
  rank INT,
  fastestLapTime STRING,
  fastestLapSpeed FLOAT,
  statusId STRING
)
USING json
OPTIONS (path "/mnt/storageaccountformulaone/raw/results.json")

-- COMMAND ----------

SELECT * FROM f1_raw.results;

-- COMMAND ----------

-- MAGIC %md 
-- MAGIC #### create pitsops table
-- MAGIC - multi line json
-- MAGIC - simple structure

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.pit_stops;
CREATE TABLE IF NOT EXISTS f1_raw.pit_stops(
  driverId INT,
  duration STRING,
  lap INT,
  milliseconds INT,
  raceId INT,
  stop INT,
  time STRING
)
USING JSON
OPTIONS (path "/mnt/storageaccountformulaone/raw/pit_stops.json", multiLine = True)

-- COMMAND ----------

SELECT * FROM f1_raw.pit_stops;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC ### create table for lists of file

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create table for lap time folder
-- MAGIC - CSV files
-- MAGIC - Multiple files

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.lap_times;
CREATE TABLE IF NOT EXISTS f1_raw.lap_times(
  raceId INT,
  driverId INT,
  lap INT,
  position INT,
  time STRING,
  milliseconds INT
)
USING CSV
OPTIONS (path "/mnt/storageaccountformulaone/raw/lap_times")

-- COMMAND ----------

SELECT * FROM  f1_raw.lap_times;

-- COMMAND ----------

-- MAGIC %md
-- MAGIC #### create qualifying table
-- MAGIC - json file
-- MAGIC - multiline files
-- MAGIC - multiple time

-- COMMAND ----------

DROP TABLE IF EXISTS f1_raw.qualifying;
CREATE TABLE IF NOT EXISTS f1_raw.qualifying (
  constructorId INT,
  driverId INT,
  number INT,
  position INT,
  q1 STRING,
  q2 STRING,
  q3 STRING,
  qualifyId INT,
  raceId INT
)
USING json
OPTIONS(path "/mnt/storageaccountformulaone/raw/qualifying", multiLine = True)

-- COMMAND ----------

SELECT * FROM f1_raw.qualifying;

-- COMMAND ----------


