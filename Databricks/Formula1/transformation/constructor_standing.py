# Databricks notebook source
# MAGIC %md
# MAGIC ### Finding out teams standing..

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

# MAGIC %md
# MAGIC #### Find race years for which the data is to be reprocessed

# COMMAND ----------

race_result_df = spark.read.format("delta").load(f'{presentation_folder_path}/race_results').filter(f"file_date = '{v_file_date}'")

# COMMAND ----------

race_year_list = df_column_to_list(race_result_df,"race_year")

# COMMAND ----------

print(race_year_list)

# COMMAND ----------

from pyspark.sql.functions import col

race_result_df = spark.read.format("delta").load(f'{presentation_folder_path}/race_results').filter(col("race_year").isin(race_year_list))

# COMMAND ----------

from pyspark.sql.functions import sum, count, when, col

# COMMAND ----------

constructor_standing_df = race_result_df\
    .groupBy('race_year','team_name')\
        .agg(sum('points').alias('total_points'),count(when(col('position') == 1,True)).alias('wins'))

# COMMAND ----------

from pyspark.sql.functions import rank, desc 
from pyspark.sql.window import Window

# COMMAND ----------

windo_spec = Window.partitionBy('race_year').orderBy(desc('total_points'), desc('wins'))

# COMMAND ----------

final_df = constructor_standing_df.withColumn('rank',rank().over(windo_spec))

# COMMAND ----------



# COMMAND ----------

merge_condition = "tgt.race_year = src.race_year and tgt.team_name = src.team_name"
merge_delta_data(final_df, "f1_presentation", "constructor_standing", presentation_folder_path, merge_condition, "race_year")

# COMMAND ----------

# overwrite_partition(final_df, "f1_presentation", "constructor_standing", "race_year")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_presentation.constructor_standing
# MAGIC order by race_year desc;

# COMMAND ----------


