# Databricks notebook source
dbutils.widgets.text("p_file_date","2021-03-21")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %md
# MAGIC ## Read all the data as required..

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run "../includes/common_utility"

# COMMAND ----------

drivers_df = spark.read.format("delta").load(f'{processed_folder_path}/drivers')\
    .withColumnRenamed('number','driver_number')\
        .withColumnRenamed('name','driver_name')\
            .withColumnRenamed('nationality','driver_nationality')

# COMMAND ----------

constructor_df = spark.read.format("delta").load(f'{processed_folder_path}/constructor')\
    .withColumnRenamed('name','team_name')

# COMMAND ----------

circuits_df = spark.read.format("delta").load(f'{processed_folder_path}/circuits')\
    .withColumnRenamed('name','circuit_name')\
        .withColumnRenamed('location','circuit_location')

# COMMAND ----------

races_df = spark.read.format("delta").load(f'{processed_folder_path}/races')\
    .withColumnRenamed('name','race_name')\
        .withColumnRenamed('race_timestamp','race_date')

# COMMAND ----------

results_df = spark.read.format("delta").load(f'{processed_folder_path}/results')\
    .filter(f"file_date = '{v_file_date}'")\
    .withColumnRenamed('time','race_time')\
    .withColumnRenamed("race_id","result_race_id")\
    .withColumnRenamed("file_date","result_file_date")    

# COMMAND ----------

# MAGIC %md
# MAGIC ### Join circuits to races
# MAGIC

# COMMAND ----------

circuit_races_df = circuits_df.join(races_df,circuits_df.circuit_id == races_df.circuit_id)\
    .select(races_df.race_id,races_df.race_year,races_df.race_name,races_df.race_date,circuits_df.circuit_location)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Join results to all other dataframe

# COMMAND ----------

race_result_df = results_df.join(circuit_races_df,results_df.result_race_id == circuit_races_df.race_id)\
                           .join(drivers_df,results_df.driver_id == drivers_df.driver_id)\
                               .join(constructor_df,results_df.constructor_id == constructor_df.constructor_id)

# COMMAND ----------

from pyspark.sql.functions import current_timestamp

# COMMAND ----------

final_df = race_result_df.select('race_year','race_id','race_name','race_date','circuit_location','driver_name','driver_number',
                                 'driver_nationality','team_name','grid','fastest_lap','race_time','points','position','result_file_date')\
                                     .withColumn('created_date',current_timestamp())\
                                     .withColumnRenamed("result_file_date","file_date")    

# COMMAND ----------

# overwrite_partition(final_df, "f1_presentation", "race_results", "race_id")

# COMMAND ----------

merge_condition = "tgt.driver_name = src.driver_name and tgt.race_id = src.race_id"
merge_delta_data(final_df, "f1_presentation", "race_results", presentation_folder_path, merge_condition, "race_id")

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id, count(1)
# MAGIC from f1_presentation.race_results
# MAGIC group by race_id
# MAGIC order by race_id desc;

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_presentation.race_results;
