# Databricks notebook source
# MAGIC %md
# MAGIC ### Produce driver standing..

# COMMAND ----------

dbutils.widgets.text("p_file_date","2021-03-28")
v_file_date = dbutils.widgets.get("p_file_date")

# COMMAND ----------

# MAGIC %run ../includes/configuration

# COMMAND ----------

# MAGIC %run ../includes/common_utility

# COMMAND ----------

# MAGIC %md
# MAGIC #### Find race year for which the data is to be processed..

# COMMAND ----------

race_result_df = spark.read.format("delta").load(f"{presentation_folder_path}/race_results").filter(f"file_date = '{v_file_date}'")

# COMMAND ----------

race_year_list = df_column_to_list(race_result_df,"race_year")

# COMMAND ----------

race_result_list

# COMMAND ----------

from pyspark.sql.functions import col


# COMMAND ----------

race_result_df = spark.read.format("delta").load(f"{presentation_folder_path}/race_results") \
    .filter(col('race_year').isin(race_year_list))

# COMMAND ----------

from pyspark.sql.functions import sum, count, when,col

# COMMAND ----------

driver_position = race_result_df\
    .groupBy('race_year','driver_name','driver_nationality')\
        .agg(sum('points').alias('total_points'),
             count(when(col('position') == 1,True)).alias('wins'))

# COMMAND ----------

from pyspark.sql.window import Window
from pyspark.sql.functions import rank, dense_rank, desc, asc
window_spec = Window.partitionBy('race_year').orderBy(desc('total_points'),asc('wins'))

# COMMAND ----------

final_df = driver_position.withColumn('rank',rank().over(window_spec))

# COMMAND ----------

# overwrite_partition(final_df, "f1_presentation", "drivers_standing", "race_year")

# COMMAND ----------

merge_condition = "tgt.driver_name = src.driver_name and tgt.race_year = src.race_year"
merge_delta_data(final_df, "f1_presentation", "drivers_standing", presentation_folder_path, merge_condition, "race_year")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from f1_presentation.drivers_standing
# MAGIC order by race_year desc;

# COMMAND ----------

# MAGIC %sql
# MAGIC -- drop table f1_presentation.drivers_standing;

# COMMAND ----------


