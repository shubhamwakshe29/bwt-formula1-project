# Databricks notebook source
# MAGIC %md
# MAGIC # Access Azure Data Lake using sas token
# MAGIC 1. set the spark config for sas token
# MAGIC 2. List files from demo container
# MAGIC 3. Read data from cicuits.csv file

# COMMAND ----------

dbutils.secrets.list(scope='formula1-screte-scope')

# COMMAND ----------

formula1_sas_key = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-sas-token-key')

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.storageaccountformulaone.dfs.core.windows.net","SAS")
spark.conf.set("fs.azure.sas.token.provider.type.storageaccountformulaone.dfs.core.windows.net","org.apache.hadoop.fs.azurebfs.sas.FixedSASTokenProvider")
spark.conf.set("fs.azure.sas.fixed.token.storageaccountformulaone.dfs.core.windows.net",formula1_sas_key)

# COMMAND ----------

dbutils.fs.ls("abfss://demo@storageaccountformulaone.dfs.core.windows.net")

# COMMAND ----------

display(spark.read.csv('abfss://demo@storageaccountformulaone.dfs.core.windows.net'))

# COMMAND ----------


