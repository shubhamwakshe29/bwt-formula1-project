# Databricks notebook source
# MAGIC %md
# MAGIC # Mount Azure Data Lake using service principal
# MAGIC 1. Get client_id, tenant_id, client_secrete from key vault.
# MAGIC 2. Set spark config with App/Client id, Directory/ Tenant ID and Secrete
# MAGIC 3. Call file system utility mount to mount the storage.
# MAGIC 4. Explore other file system utility related to mount (lists all mount,unmount)

# COMMAND ----------

formula1_client_id = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-client-id')
formula1_tenant_id = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-tenant-id')
formula1_client_secrete = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-client-secrete')

# COMMAND ----------

configs = {"fs.azure.account.auth.type": "OAuth",
          "fs.azure.account.oauth.provider.type": "org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider",
          "fs.azure.account.oauth2.client.id": formula1_client_id,
          "fs.azure.account.oauth2.client.secret": formula1_client_secrete,
          "fs.azure.account.oauth2.client.endpoint": f"https://login.microsoftonline.com/{formula1_tenant_id}/oauth2/token"}


# COMMAND ----------

dbutils.fs.mount(
  source = "abfss://demo@storageaccountformulaone.dfs.core.windows.net/",
  mount_point = "/mnt/storageaccountformulaone/demo",
  extra_configs = configs)

# COMMAND ----------

display(dbutils.fs.ls("/mnt/storageaccountformulaone/demo"))

# COMMAND ----------

display(spark.read.csv('/mnt/storageaccountformulaone/demo'))

# COMMAND ----------

display(dbutils.fs.mounts())

# COMMAND ----------

dbutils.fs.unmount('/mnt/storageaccountformulaone/demo')

# COMMAND ----------


