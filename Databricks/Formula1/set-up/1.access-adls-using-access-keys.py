# Databricks notebook source
# MAGIC %md
# MAGIC # Access Azure Data Lake using access keys
# MAGIC 1. set the spark config fs.azure.account.key
# MAGIC 2. List files from demo container
# MAGIC 3. Read data from cicuits.csv file

# COMMAND ----------

formula1_account_key = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-account-key')

# COMMAND ----------

spark.conf.set(
    'fs.azure.account.key.storageaccountformulaone.dfs.core.windows.net',
    formula1_account_key
)

# COMMAND ----------

display(dbutils.fs.ls('abfss://demo@storageaccountformulaone.dfs.core.windows.net'))

# COMMAND ----------

display(spark.read.csv('abfss://demo@storageaccountformulaone.dfs.core.windows.net'))

# COMMAND ----------


