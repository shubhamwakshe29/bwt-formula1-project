# Databricks notebook source
# MAGIC %md
# MAGIC Explore DBFS root
# MAGIC 1. List all the folders in dbfs roots
# MAGIC 2. Interact with DBFS file browser
# MAGIC 3. Upload file to DBFS root

# COMMAND ----------

display(dbutils.fs.ls('/'))

# COMMAND ----------

display(dbutils.fs.ls('/FileStore'))

# COMMAND ----------

display(spark.read.csv('dbfs:/FileStore/002_circuits.csv'))

# COMMAND ----------


