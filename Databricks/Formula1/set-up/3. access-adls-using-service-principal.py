# Databricks notebook source
# MAGIC %md
# MAGIC # Access Azure Data Lake using service principal
# MAGIC 1. Register Azure Actice Directory(AD) application / service principal
# MAGIC 2. Generate a secrete / password for the application
# MAGIC 3. Set spark config with App/Client id, Directory/ Tenant ID and Secrete
# MAGIC 4. Assign role 'Storage Blob Data Contributor' to the data lake.

# COMMAND ----------

dbutils.secrets.list(scope='formula1-screte-scope')

# COMMAND ----------

formula1_client_id = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-client-id')
formula1_tenant_id = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-tenant-id')
formula1_client_secrete = dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-client-secrete')

# COMMAND ----------

spark.conf.set("fs.azure.account.auth.type.storageaccountformulaone.dfs.core.windows.net","OAuth")
spark.conf.set("fs.azure.account.oauth.provider.type.storageaccountformulaone.dfs.core.windows.net","org.apache.hadoop.fs.azurebfs.oauth2.ClientCredsTokenProvider")
spark.conf.set("fs.azure.account.oauth2.client.id.storageaccountformulaone.dfs.core.windows.net",formula1_client_id)
spark.conf.set("fs.azure.account.oauth2.client.secret.storageaccountformulaone.dfs.core.windows.net",formula1_client_secrete)
spark.conf.set("fs.azure.account.oauth2.client.endpoint.storageaccountformulaone.dfs.core.windows.net",f"https://login.microsoftonline.com/{formula1_tenant_id}/oauth2/token")

# COMMAND ----------

dbutils.fs.ls("abfss://demo@storageaccountformulaone.dfs.core.windows.net")

# COMMAND ----------

display(spark.read.csv('abfss://demo@storageaccountformulaone.dfs.core.windows.net'))

# COMMAND ----------


