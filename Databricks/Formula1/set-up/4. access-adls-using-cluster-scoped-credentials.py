# Databricks notebook source
# MAGIC %md
# MAGIC # Access Azure Data Lake using cluster scoped credentials
# MAGIC 1. set the spark config fs.azure.account.key in the cluster
# MAGIC 2. List files from demo container
# MAGIC 3. Read data from cicuits.csv file

# COMMAND ----------

display(dbutils.fs.ls('abfss://demo@storageaccountformulaone.dfs.core.windows.net'))

# COMMAND ----------

display(spark.read.csv('abfss://demo@storageaccountformulaone.dfs.core.windows.net'))

# COMMAND ----------


