# Databricks notebook source
# MAGIC %md
# MAGIC ### Explore the capabilities of dbutils.secretes utility

# COMMAND ----------

dbutils.secrets.help()

# COMMAND ----------

dbutils.secrets.listScopes()

# COMMAND ----------

dbutils.secrets.list(scope= 'formula1-screte-scope')

# COMMAND ----------

dbutils.secrets.get(scope='formula1-screte-scope',key='formula1-account-key')

# COMMAND ----------


